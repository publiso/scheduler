import prisma from './prisma';
import startServer from './server';
import { fetch, POOL, mark } from './runner';
import { Twitter } from './socialMedia/twitter';
import TwitterApi from 'twitter-api-v2';

interface PromiseFulfilledResult<T> {
    status: 'fulfilled';
    value: T;
}

interface PromiseRejectedResult {
    status: 'rejected';
    reason: any;
}

type PromiseSettledResult<T> = PromiseFulfilledResult<T> | PromiseRejectedResult;

function timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function notEmpty<Twitter>(value: Twitter | null | undefined): value is Twitter {
    if (value === null || value === undefined) return false;
    // ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const testDummy: Twitter = value;
    return true;
}

function valueNotEmpty(obj: any): boolean {
    if (obj.value === null || obj.value === undefined) return false;
    return true;
}

async function mainLoop() {
    const date = new Date();
    console.log(`Cycle starts at ${date.toString()}`);
    const requests = await fetch();
    const filtered: Twitter[] = requests.filter(notEmpty);
    const len = filtered.length;
    if (len > 0) console.log(`${len} new job found`);

    const checkPool = filtered.map((req) => POOL(req.send));
    const sent: PromiseSettledResult<any>[] = await Promise.allSettled(checkPool);
    const c = sent.filter(valueNotEmpty);
    console.log(`${c.length} request successful, ${len - c.length} failed.`);
    sent.forEach(async (message) => await mark(message));

    console.log(`Cycle ends after ${new Date().valueOf() - date.valueOf()} millis`);
}

async function main() {
    startServer();

    // eslint-disable-next-line no-constant-condition
    while (true) {
        await mainLoop();
        await timeout(5 * 1000);
    }
}


main()
    .catch(e => console.error(e))
    .finally(async () => { await prisma.$disconnect(); });
