import prisma from './prisma';
import startServer from './server';
import { fetch, POOL, mark } from './runner';
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function notEmpty(value) {
    if (value === null || value === undefined)
        return false;
    // ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const testDummy = value;
    return true;
}
function valueNotEmpty(obj) {
    if (obj.value === null || obj.value === undefined)
        return false;
    return true;
}
async function mainLoop() {
    const date = new Date();
    console.log(`Cycle starts at ${date.toString()}`);
    const requests = await fetch();
    const filtered = requests.filter(notEmpty);
    console.log(filtered);
    const len = filtered.length;
    if (len > 0)
        console.log(`${len} new job found`);
    const checkPool = filtered.map((req) => POOL(req.send));
    const sent = await Promise.allSettled(checkPool);
    const c = sent.filter(valueNotEmpty);
    console.log(`${c.length} request successful, ${len - c.length} failed.`);
    sent.forEach(async (message) => await mark(message));
    console.log(`Cycle ends after ${new Date().valueOf() - date.valueOf()} millis`);
}
async function main() {
    startServer();
    // eslint-disable-next-line no-constant-condition
    while (true) {
        await mainLoop();
        await timeout(5 * 1000);
    }
}
main()
    .catch(e => console.error(e))
    .finally(async () => { await prisma.$disconnect(); });
