import TwitterApi from 'twitter-api-v2';
export class Twitter {
    id;
    userId;
    idTim;
    status;
    tanggalWaktuPublikasi;
    judulPublikasi;
    bahanKonten;
    deskripsiDesainPublikasi;
    buktiMendesak;
    namaMediaSosial;
    credential;
    send;
    constructor(query) {
        this.id = query.id;
        this.userId = query.userId;
        this.idTim = query.idTim;
        this.status = query.status;
        this.tanggalWaktuPublikasi = query.tanggalWaktuPublikasi;
        this.judulPublikasi = query.judulPublikasi;
        this.bahanKonten = query.bahanKonten;
        this.deskripsiDesainPublikasi = query.deskripsiDesainPublikasi;
        this.buktiMendesak = query.buktiMendesak;
        this.namaMediaSosial = query.namaMediaSosial;
        let cred = JSON.stringify(query.credential);
        cred = JSON.parse(cred);
        this.credential = cred;
        this.send = async () => {
            // @ts-ignore
            const twitterClient = new TwitterApi.TwitterApi({
                appKey: this.credential.appKey,
                appSecret: this.credential.appSecret,
                accessToken: this.credential.accessToken,
                accessSecret: this.credential.accessSecret,
            });
            try {
                const result = await twitterClient.v2.tweet(this.bahanKonten);
                return {
                    'result': result,
                    'id': this.id
                };
            }
            catch (error) {
                console.log(error);
                return null;
            }
        };
    }
}
