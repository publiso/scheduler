import { Query } from './interface';
import { Status } from '@prisma/client/index';
import TwitterApi, { TweetV2PostTweetResult } from 'twitter-api-v2';

export interface TwitterCredential {
    appKey: string;
    appSecret: string;
    accessToken: string;
    accessSecret: string;
}

export interface ITwitter extends Omit<Query, 'credential'> {
    credential: TwitterCredential;
}

export class Twitter implements ITwitter {
    id!: number;
    userId!: string;
    idTim!: number;
    status!: Status;
    tanggalWaktuPublikasi!: Date;
    judulPublikasi!: string;
    bahanKonten!: string;
    deskripsiDesainPublikasi!: string;
    buktiMendesak!: string | null;
    namaMediaSosial!: string;
    credential!: TwitterCredential;
    send: () => void;

    constructor(query: Query) {
        this.id = query.id;
        this.userId = query.userId;
        this.idTim = query.idTim;
        this.status = query.status;
        this.tanggalWaktuPublikasi = query.tanggalWaktuPublikasi;
        this.judulPublikasi = query.judulPublikasi;
        this.bahanKonten = query.bahanKonten;
        this.deskripsiDesainPublikasi = query.deskripsiDesainPublikasi;
        this.buktiMendesak = query.buktiMendesak;
        this.namaMediaSosial = query.namaMediaSosial;

        let cred = JSON.stringify(query.credential);
        cred = JSON.parse(cred);
        this.credential = (cred as unknown) as TwitterCredential;

        this.send = async () => {
            // @ts-ignore
            const twitterClient = new TwitterApi.TwitterApi({
                appKey: this.credential.appKey,
                appSecret: this.credential.appSecret,
                accessToken: this.credential.accessToken,
                accessSecret: this.credential.accessSecret,
            });
            try {
                const result = await twitterClient.v2.tweet(this.bahanKonten);
                return {
                    'result': result,
                    'id': this.id
                };
            } catch (error) {
                console.log(error);
                return null;
            }
        };
    }
}