import prisma from './prisma';
import { Query } from './socialMedia/interface';
import { Twitter } from './socialMedia/twitter';
import plimit, { LimitFunction } from 'p-limit';


export const POOL: LimitFunction = plimit(5);

export async function mark(result: any) {
    if (result.value === undefined || result.value === null) {
        console.log('Failed to send publication');
        return;
    }
    const query = result.value;
    console.log('Updating', result);
    // const requests = await prisma.$queryRaw`
    // UPDATE "PermintaanPublikasi"
    // SET status = 'selesai'::"Status"
    // WHERE id = ${1};`;
    try {
        const requests = await prisma.permintaanPublikasi.update({
            where: { id: query.id },
            data: {
                status: 'selesai'
            }
        });
        console.log(requests);
    } catch (error) {
        console.log('FAILED to update db');
        console.error(error);
    }
}

export async function fetch() {

    const requests = await prisma.$queryRaw`
    SELECT
        p.id,
        p."userId",
        p."idTim",
        p.status,
        p."tanggalWaktuPublikasi", 
        p."judulPublikasi", 
        p."bahanKonten", 
        m."namaMediaSosial", 
        m.credential
    FROM
        "PermintaanPublikasi" p
        JOIN "MediaSosialUntukPermintaanPublikasi" mp
            ON p.id = mp."idPermintaanPublikasi"
        JOIN "MediaSosial" m
            ON m.id = mp."idPermintaanPublikasi"
    WHERE p.status = 'dijadwalkan'::"Status"
        AND NOW() >= p."tanggalWaktuPublikasi";
        ` as Query[];

    console.log(requests);

    return requests.map((query: Query) => {
        console.log('Found', query.namaMediaSosial);
        switch (query.namaMediaSosial) {
            case 'twitter':
                return new Twitter(query);
            default:
                return null;
        }
    });

}

