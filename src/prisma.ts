import PrismaClientPkg from '@prisma/client';

const client = new PrismaClientPkg.PrismaClient();

export default client;