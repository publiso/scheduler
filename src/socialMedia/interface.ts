import { Status, Prisma } from '@prisma/client/index';


export type Query = {
    id: number
    userId: string
    idTim: number
    status: Status
    tanggalWaktuPublikasi: Date
    judulPublikasi: string
    bahanKonten: string
    deskripsiDesainPublikasi: string
    buktiMendesak: string | null
    namaMediaSosial: string
    credential: Prisma.JsonObject
}


export interface Broadcast extends Query {
    send: () => void
}