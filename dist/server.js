'use strict';
import express from 'express';
const app = express();
const PORT = process.env.PORT || 8080;
export default function startServer() {
    app.get('/', (req, res) => {
        res.send('pong!');
    });
    app.get('*', function (req, res) {
        res.status(404).send();
    });
    app.listen(PORT, () => {
        console.log(`Server is listening on http://127.0.0.1:${PORT}/`);
    });
}
